//
//  CardModel.swift
//  ParentAndModelPOC
//
//  Created by aravind-zt336 on 10/04/20.
//

import Foundation

// MARK: - CardModel
class CardModel: Codable {
    var name: String
    var elements: [Element]// ROWS KEYS
    var childrens: [Children]//ROWS DEFINITION

    enum CodingKeys: String, CodingKey {
        case name
        case elements = "Elements"
        case childrens
    }

    init(name: String, elements: [Element], childrens: [Children]) {
        self.name = name
        self.elements = elements
        self.childrens = childrens
    }
}

// MARK: - Children
class Children: Codable {
    var name: String
    var haveChildrens: Bool
    var components: Components?
    var orientation: String?
    var columnSet: [ColumnSet]?

    init(name: String, haveChildrens: Bool, components: Components?, orientation: String?, columnSet: [ColumnSet]?) {
        self.name = name
        self.haveChildrens = haveChildrens
        self.components = components
        self.orientation = orientation
        self.columnSet = columnSet
    }
}

// MARK: - ColumnSet
class ColumnSet: Codable {
    var name: String

    init(name: String) {
        self.name = name
    }
}

// MARK: - Components
class Components: Codable {
    var type, key: String
    var size: Size
    var style: Style

    init(type: String, key: String, size: Size, style: Style) {
        self.type = type
        self.key = key
        self.size = size
        self.style = style
    }
}

// MARK: - Size
class Size: Codable {
    var width: String
    var height: Int

    init(width: String, height: Int) {
        self.width = width
        self.height = height
    }
}

// MARK: - Style
class Style: Codable {
    var allignment: String

    init(allignment: String) {
        self.allignment = allignment
    }
}

// MARK: - Element
class Element: Codable {
    var childKey: String

    init(childKey: String) {
        self.childKey = childKey
    }
}


// MARK: - CardModel
class CardModel2: Codable {
    let body: [CardModelRow]

    init(body: [CardModelRow]) {
        self.body = body
    }
}

// MARK: - CardModelRow
class CardModelRow: Codable {
    let key: String
    let rows: [Row]

    init(key: String, rows: [Row]) {
        self.key = key
        self.rows = rows
    }
}

// MARK: - RowRow
class Row: Codable {
    let column: Column
    
    init(column: Column ) {
        self.column = column
        
    }
}

// MARK: - Column
class Column: Codable {
    let key: String
    let rows: [Row]?
    init(key: String, rows: [Row]?) {
        self.key = key
        self.rows = rows
    }
}
