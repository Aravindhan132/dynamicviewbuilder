//
//  Utilities.swift
//  ParentAndModelPOC
//
//  Created by aravind-zt336 on 10/04/20.
//

import Foundation
import UIKit

public enum ComponentType: String {
    case label = "label"
    case imageView = "ImageView"
}

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}
extension UIColor {
    static func random() -> UIColor {
        return UIColor(
           red:   .random(),
           green: .random(),
           blue:  .random(),
           alpha: 1.0
        )
    }
}
