//
//  ViewController.swift
//  ParentAndModelPOC
//
//  Created by aravind-zt336 on 10/04/20.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet var adaptiveTable: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = "Adaptive UI"
        
        prepareTable()
        saveToDB()
        
    }
    
    
    fileprivate func saveToDB() {
        BuilderModelGenerator.generate { (cardmodel) in
            CoreDataHelper.saveCoreDB(model: cardmodel)
        }
    }
    

    
    fileprivate func prepareTable() -> Void{
        adaptiveTable.dataSource = self
        adaptiveTable.delegate = self
        adaptiveTable.separatorStyle = .singleLine
        adaptiveTable.rowHeight = 200
    }
    

}


extension UIViewController: UITableViewDataSource , UITableViewDelegate {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: tableView.rowHeight)
        let componentView = ComponentGeneratedView(frame: frame)
        cell.addSubview(componentView)
        return cell
    }
}
