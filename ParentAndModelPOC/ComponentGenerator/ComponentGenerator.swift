//
//  ComponentGenerator.swift
//  ParentAndModelPOC
//
//

import Foundation
import UIKit
class ComponentGeneratedView: UIView {
    
    var cardModel: CardModel?
       
    override init(frame: CGRect) {
        super.init(frame: frame)
        BuilderModelGenerator.generate { (cardModel) in
            self.cardModel = cardModel
            self.prepareActualRows(rowModel: cardModel.elements)//Rows are declared as Elements
        }
    }
    
    
    fileprivate func prepareActualRows(rowModel: [Element]) {
        var subViews: [UIView] = [UIView]()
        
        rowModel.forEach({ (eachRows) in
            let eachRowElement = self.cardModel?.childrens.filter({$0.name == eachRows.childKey}).first
            
            let rowView = self.prepareRowComponents(for: eachRowElement, stack: nil)
            subViews.append(rowView)
           
        })
        
        let stackView = UIStackView(arrangedSubviews: subViews)
        stackView.distribution = .fillProportionally
        stackView.axis = .vertical
        stackView.frame = CGRect(x: .zero, y: .zero, width: self.frame.width, height: self.frame.height)
        self.addSubview(stackView)
    }
    
  
    @discardableResult fileprivate func prepareRowComponents(for model: Children? , stack: UIStackView?) -> UIView {
        
        if (model?.haveChildrens ?? false) {
            let view = createViewWithColumns(for: model)
            view.backgroundColor = .green
            stack?.addArrangedSubview(view)
            return view
            
            
        } else {
             stack?.addArrangedSubview(prepareElementView(for: model))
             return prepareElementView(for: model)
        }
        

    }
    
    @discardableResult fileprivate func createViewWithColumns(for model: Children?) -> UIView {
        let columnHolderView: UIStackView = UIStackView()
        
        columnHolderView.axis = model?.orientation == "vertical" ?  .vertical : .horizontal
        columnHolderView.distribution = .fillProportionally
        
        model?.columnSet?.forEach({ (eachColumn) in
            let eachRowElement = self.cardModel?.childrens.filter({$0.name == eachColumn.name}).first
            self.prepareRowComponents(for: eachRowElement, stack: columnHolderView)
        })
        
        
        return columnHolderView
    }
    
    
    @discardableResult fileprivate func prepareElementView(for model: Children?) -> UIView {
        let elementSubView = self.prepareElementSubViews(for: model)
        let stackView = UIStackView(arrangedSubviews: elementSubView)
        stackView.axis = .horizontal
        return stackView
    }
    
    @discardableResult fileprivate func prepareElementSubViews(for model: Children?) -> [UIView] {
        let component = model?.components
        let type = component?.type
        
        var subViews: [UIView] = [UIView]()
        subViews.removeAll()
        
        switch type{
        case "label":
            let label = UILabel()
            label.text = model?.components?.key
            label.textAlignment = .left
            label.backgroundColor = .random()
            subViews.append(label)
            
        default:
            let view = UIView()
            subViews.append(view)
        }
        
        
        return subViews
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}



extension UIStackView {
    func addBackground(color: UIColor) {
        let subView = UIView(frame: bounds)
        subView.backgroundColor = color
        subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(subView, at: 0)
    }
}


