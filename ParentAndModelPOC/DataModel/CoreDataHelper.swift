//
//  CoreDataHelper.swift
//  ParentAndModelPOC
//
//  Created by aravind-zt336 on 13/04/20.
//  Copyright © 2020 Zoho. All rights reserved.
//

import Foundation
import CoreData

public class CoreDataHelper: NSObject {

    static let CARD_ENTITY = "Card"
    static let CCHILDREN_ENTITY = "CChildren"
    static let CCOLUMNSET_PATTERN_ENTITY = "CColumnSet"
    static let CCOMPONENT_ENTITY = "CComponents"
    static let CELEMENT_ENTITY = "CElement"
    static let CSIZE_ENTITY = "CSize"
    static let ZSTYLE_ENTITY = "CStyle"
    
    
    static func newCardEntityObject(withContext context : NSManagedObjectContext = CoreDataHandler.sharedInstance.getPrivateContext()) -> Card? {
        let formEntity = NSEntityDescription.insertNewObject(forEntityName: CoreDataHelper.CARD_ENTITY, into: context) as? Card
        return formEntity
    }

    static func newChildrenItemEntityObject(withContext context : NSManagedObjectContext = CoreDataHandler.sharedInstance.getPrivateContext()) -> CChildren? {
        let navigationItemEntity = NSEntityDescription.insertNewObject(forEntityName: CoreDataHelper.CCHILDREN_ENTITY, into: context) as? CChildren
        return navigationItemEntity
    }

    static func newColumnSetPatternEntityObject(withContext context : NSManagedObjectContext = CoreDataHandler.sharedInstance.getPrivateContext()) -> CColumnSet? {
        let formFieldPatternEntity = NSEntityDescription.insertNewObject(forEntityName: CoreDataHelper.CCOLUMNSET_PATTERN_ENTITY, into: context) as? CColumnSet
        return formFieldPatternEntity
    }

    static func newComponentEntityObject(withContext context : NSManagedObjectContext = CoreDataHandler.sharedInstance.getPrivateContext()) -> CComponents? {
        let actionEntity = NSEntityDescription.insertNewObject(forEntityName: CoreDataHelper.CCOMPONENT_ENTITY, into: context) as? CComponents
        return actionEntity
    }

    static func newElementEntityObject(withContext context : NSManagedObjectContext = CoreDataHandler.sharedInstance.getPrivateContext()) -> CElement? {
        let fieldEntity = NSEntityDescription.insertNewObject(forEntityName: CoreDataHelper.CELEMENT_ENTITY, into: context) as? CElement
        return fieldEntity
    }

    static func newSizeEntityObject(withContext context : NSManagedObjectContext = CoreDataHandler.sharedInstance.getPrivateContext()) -> CSize? {
        let navigationEntity = NSEntityDescription.insertNewObject(forEntityName: CoreDataHelper.CSIZE_ENTITY, into: context) as? CSize
        return navigationEntity
    }
    
    static func newStyleEntityObject(withContext context : NSManagedObjectContext = CoreDataHandler.sharedInstance.getPrivateContext()) -> CStyle? {
        let navigationEntity = NSEntityDescription.insertNewObject(forEntityName: CoreDataHelper.ZSTYLE_ENTITY, into: context) as? CStyle
        return navigationEntity
    }
    

    //================= ****************** ================//
    
    static func fetchLayouts(of context: NSManagedObjectContext = CoreDataHandler.sharedInstance.getMainContext()) -> [Card]? {
        let fetchRequest: NSFetchRequest<Card> = Card.fetchRequest()
        guard let result = try? context.fetch(fetchRequest), result.count > 0 else { return nil }
        return result
    }
    
    
    static func insertCard(_ layouts: CardModel?, onComplition:(() -> ())? = nil) {
           
        CoreDataHandler.sharedInstance.persistentContainer.performBackgroundTask { (context) in
            
            layouts?.elements.forEach({ (layout) in
                let card = CoreDataHelper.newCardEntityObject(withContext: context)
                card?.name = layout.childKey
            })
            
            layouts?.childrens.forEach({ (eachChildren) in
                let child = CoreDataHelper.newChildrenItemEntityObject(withContext: context)
                child?.name = eachChildren.name
                child?.haveChildren = eachChildren.haveChildrens
                child?.orientation = eachChildren.orientation
                
                eachChildren.columnSet?.forEach({ (eachColumn) in
                    let column = CoreDataHelper.newColumnSetPatternEntityObject(withContext: context)
                    column?.name = eachChildren.name
                })
                
                let component = CoreDataHelper.newComponentEntityObject(withContext: context)
                component?.type = eachChildren.components?.type
                component?.key = eachChildren.components?.key
               
                
                let size = CoreDataHelper.newSizeEntityObject(withContext: context)
                size?.height =  ""//eachChildren.components?.size.height
                size?.width = String(eachChildren.components?.size.width ?? "")
                
                let style = CoreDataHelper.newStyleEntityObject(withContext: context)
                style?.allignment = eachChildren.components?.style.allignment
            })
            
            context.saveContext()
            onComplition?()
        }
           
    }
    
    
    static func deleteCards(of context: NSManagedObjectContext = CoreDataHandler.sharedInstance.getMainContext(), onComplition:(() -> ())? = nil) {
           
           let fetchRequest: NSFetchRequest<Card> = Card.fetchRequest()
           let objects = try? context.fetch(fetchRequest)
           objects?.forEach({ (form) in
               context.delete(form)
           })
           context.saveContext()
           onComplition?()
       }
    
    
    static func saveCoreDB(model: CardModel) {
        CoreDataHelper.deleteCards {
            CoreDataHelper.insertCard(model) {
                let layout = CoreDataHelper.fetchLayouts()
                print(layout ?? "")
            }
        }
    }
    
    
}
