//
//  BuilderModelGenerator.swift
//  ParentAndModelPOC
//
//  Created by aravind-zt336 on 10/04/20.
//

import Foundation
class BuilderModelGenerator {
    static func generate(_ onCompletion: ((CardModel) -> Void)? = nil) {
        guard let url = Bundle.main.url(forResource: "layout", withExtension: "json") else { return }

        guard let data = try? Data(contentsOf: url) else { return }
        guard let jsonData = try? JSONDecoder().decode(CardModel.self, from: data) else { return }
        
        onCompletion?(jsonData)
    }
    
    
   static func generate2(_ onCompletion: ((CardModel2) -> Void)? = nil) {
        guard let url2 = Bundle.main.url(forResource: "layout2", withExtension: "json") else { return }

        guard let data2 = try? Data(contentsOf: url2) else { return }
        guard let jsonData2 = try? JSONDecoder().decode(CardModel2.self, from: data2) else { return }
        
        onCompletion?(jsonData2)
    }
    
}
